﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo
{
    public class SortName
    {
        public List<Product> sortName(List<Product> products)
        {
            int holePosition;
            Product valueToInsert;
            for (int i = 0; i <= products.Count - 1; i++)
            {
                valueToInsert = products[i];
                holePosition = i;
                while (holePosition > 0 && valueToInsert.Name.Length < products[holePosition - 1].Name.Length)
                {
                    products[holePosition] = products[holePosition - 1];
                    holePosition = holePosition - 1;
                }
                products[holePosition] = valueToInsert;
            }
            return products;
        }
    }
}
