﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> products = new List<Product>()
            {
                new Product()
                {
                    Name = "CPU", Price = 750, Quality = 10, CategoryId = 1
                },
                new Product()
                {
                    Name = "RAM", Price = 50, Quality = 2, CategoryId = 2
                },
                new Product()
                {
                    Name = "HDD", Price = 70, Quality = 1, CategoryId = 2
                },
                new Product()
                {
                    Name = "Main", Price = 400, Quality = 3, CategoryId = 1
                },
                new Product()
                {
                    Name = "Keyboard", Price = 30, Quality = 8, CategoryId = 4
                },
                new Product()
                {
                    Name = "Mouse", Price = 25, Quality = 50, CategoryId = 4
                },
                new Product()
                {
                    Name = "VGA", Price = 60, Quality = 35, CategoryId = 3
                },
                new Product()
                {
                    Name = "Monitor", Price = 120, Quality = 28, CategoryId = 2
                },
                new Product()
                {
                    Name = "Case", Price = 120, Quality = 28, CategoryId = 5
                }
            };

            List<Category> categories = new List<Category>
            {
                new Category()
                {
                    ID = 1, Name = "Comuter"
                },
                new Category()
                {
                    ID = 2, Name = "Memory"
                },
                new Category()
                {
                    ID = 3, Name = "Card"
                },
                new Category()
                {
                    ID = 4, Name = "Acsesory"
                }
            };
            var method = new MoneyWithRecursion();
            var method2 = new MoneyWithoutRecursion();
            method.CalMonth(20, 5, 40, 0);
            Console.WriteLine("Month by Recursion: " + method.CalMonth(20, 5, 40, 0));
            Console.WriteLine("Month not by Recursion: " + method2.CalMonth(20, 5));
            Console.Read();
        }
    }
}
