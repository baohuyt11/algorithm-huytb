﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo
{
    public class Search
    {
        public static void searchName(List<Product> products, string name)
        {
            foreach (Product product in products)
            {
                if (product.Name == name)
                {
                    Console.WriteLine(product);
                }
            }
        }

        public static void searchCateID(List<Product> products, int cateID)
        {
            foreach (Product product in products)
            {
                if (product.CategoryId == cateID)
                {
                    Console.WriteLine(product);
                }
            }
        }
        public static void searchPrice(List<Product> products, int price)
        {
            foreach (Product product in products)
            {
                if (product.Price <= 100)
                {
                    Console.WriteLine(product);
                }
            }
        }
    }
}
