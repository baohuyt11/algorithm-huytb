﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algo
{
    public class MoneyWithRecursion
    {
        public double CalMonth(double money, double rate, double total, int month)
        {
            if (money >= total) return month;
            month++;
            return CalMonth(money * (1 + (rate / 100)), rate, total, month);
        }
    }

    public class MoneyWithoutRecursion
    {
        public double CalMonth(double money, double rate)
        {
            double total = money * 2;
            int month = 0;
            while (total > money)
            {
                money = money * (1 + (rate / 100));
                month++;
            }
            return month;
        }
    }
}
