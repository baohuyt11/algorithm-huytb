﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algo
{
    public class SalaryWithRecursion
    {
        public double CalSalary(double salary, int n)
        {
            if (n == 0) return salary;
            return CalSalary(salary, n - 1) * 1.1;
        }
    }
    public class SalaryWithoutRecursion
    {
        public double CalSalary(double salary, int n)
        {
            List<double> Sal = new List<double>();
            for (int i = 0; i < n; i++)
            {
                double newSalary = salary + (salary * 0.1);
                salary = newSalary;
                Sal.Add(salary);
            }
            double last = Sal[Sal.Count - 1];
            return last;
        }
    }
}
