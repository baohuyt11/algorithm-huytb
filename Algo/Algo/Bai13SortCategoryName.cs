﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo
{
    public class SortCategoryName
    {
        public List<Product> sortCategoryName(List<Product> products, List<Category> categories)
        {
            // sort product by name using insertion sort
            int holePosition;
            Product valueToInsert;
            for (int i = 0; i <= products.Count - 1; i++)
            {
                valueToInsert = products[i];
                holePosition = i;
                while (holePosition > 0 && valueToInsert.Name[0] < products[holePosition - 1].Name[0])
                {
                    products[holePosition] = products[holePosition - 1];
                    holePosition = holePosition - 1;
                }
                products[holePosition] = valueToInsert;
            }

            // add sort product by name to new list
            var Sort = new List<Product>();
            foreach (Product p in products)
            {
                foreach (Category c in categories)
                {
                    if (p.CategoryId == c.ID)
                    {
                        Sort.Add(p);
                    }
                }
            }
            return Sort;
        }
    }
}
