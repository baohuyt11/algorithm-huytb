using System;

namespace Algo
{
    public class Product
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public int Quality { get; set; }
        public int CategoryId { get; set; }

        public override string ToString()
        {
            return "Name: " + Name + " | Price: " + Price + " | Quality: " + Quality + " | CategoryId: " + CategoryId;
        }
    }
}
