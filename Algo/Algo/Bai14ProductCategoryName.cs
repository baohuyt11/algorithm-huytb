﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo
{
	public class Map
	{
		public List<ProductCategory> mapProductByCategory(List<Product> products, List<Category> categories)
		{
			List<ProductCategory> productCategories = new List<ProductCategory>();
            foreach (Category c in categories)
            {
                foreach (Product p in products)
                {
                    if (p.CategoryId == c.ID)
                    {
                        productCategories.Add(new ProductCategory { Name = p.Name, Price = p.Price, Quality = p.Quality, CategoryName = c.Name });
                    }
                }
            }
            return productCategories;

        }
	}
}

