﻿using System;

namespace Algo
{
    public class ProductCategory
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public int Quality { get; set; }
        public string CategoryName { get; set; }

        public override string ToString()
        {
            return "Name: " + Name + " | Price: " + Price + " | Quality: " + Quality + " | CategoryName: " + CategoryName;
        }
    }
}
