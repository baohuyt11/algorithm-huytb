﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo
{
    public class SortPrice
    {
        public List<Product> sortPrice(List<Product> products)
        {
            int temp;
            for (int j = 0; j <= products.Count - 2; j++)
            {
                for (int i = 0; i <= products.Count - 2; i++)
                {
                    if (products[i].Price > products[i + 1].Price)
                    {
                        temp = products[i].Price;
                        products[i].Price = products[i + 1].Price;
                        products[i + 1].Price = temp;
                    }
                }
            }
            return products;
        }
    }
}

